FROM nvidia/cuda:11.0-devel-ubuntu20.04
MAINTAINER Viacheslav Ostroukh <V.Ostroukh@tudelft.nl>

# make our environment sane
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        git curl pandoc python3-all-dev python3-pip python3-setuptools \
        python3-wheel python3-tk \
        # Additional tools for running CI
        file rsync openssh-client && \
    echo UTC > /etc/timezone && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install --upgrade --no-cache pip && \
    pip install --upgrade --no-cache \
        ipython \
        ipykernel \
        jupyter_client \
        matplotlib \
        nbconvert \
        nbsphinx \
        numpy \
        numpydoc \
        pyyaml \
        parsimonious \
        pytools \
        scipy \
        sympy \
        toolz \
        xarray \
        sphinx && \
    pip install --upgrade --no-cache pycuda

# auxillary dependencies
RUN pip install --upgrade --no-cache \
        codecov \
        pytest \
        pylint \
        pytest-cov \
        twine

RUN ssh-keyscan -t rsa gitlab.com >> /etc/ssh/ssh_known_hosts && \
    ssh-keyscan -t rsa github.com >> /etc/ssh/ssh_known_hosts
